<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PostGroupRepository")
 */
class PostGroup
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $group_title;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGroupTitle(): ?string
    {
        return $this->group_title;
    }

    public function setGroupTitle(?string $group_title): self
    {
        $this->group_title = $group_title;

        return $this;
    }
}
