<?php

namespace App\Controller;

use App\Entity\PostGroup;
use App\Repository\PostGroupRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\PostGroupType;
use Symfony\Component\HttpFoundation\RedirectResponse;
  /**
     * @Route("/post_group", name="post.")
     */
class PostGroupController extends AbstractController
{
    /**
     * @Route("/", name="post_group_index")
     * @param PostGroupRepository $PostGroupRepository
     * @return Response
     */

    public function index(PostGroupRepository $PostGroupRepository)
    {

        $post_group = $PostGroupRepository->findAll();
   
        return $this->render('post_group/index.html.twig', [
            'post_group' => $post_group
        ]);
    }

    /**
     * @Route("/creategroup", name="creategroup")
     * @param Request $request
     * @return Response
     */
    public function creategroup(Request $request){
        $post_group = new PostGroup();
       // $post->setTitle('toks pavadinimas');
       $form = $this->createForm(PostGroupType::class, $post_group);
        $form->handleRequest($request);
        if($form->isSubmitted()){
            $em = $this->getDoctrine()->getManager();
            //dump($post);
            $em->persist($post_group);
            $em->flush();
           // return $this->redirect($this->generateUrl('post_group.index.twig'));
        }
       // return $this->redirect($this->generateUrl('post.index'));
       return $this->render('post_group/creategroup.html.twig', ['form' => $form->createView()
       ]);
    }


    /**
     * @Route("/deletegroup/{id}", name="deletegroup")
     * @return Response
     */
    public function remove(PostGroupRepository $PostGroupRepository,PostGroup $post_group){
   
        $em = $this->getDoctrine()->getManager();
        $em->remove($post_group);
        $em->flush();

       
        $post_group = $PostGroupRepository->findAll();
        $this->addFlash('success', 'PostGroup was removed');
        return $this->render('post_group/index.html.twig', [
            'post_group' => $post_group
        ]);
    }



}
