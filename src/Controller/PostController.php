<?php

namespace App\Controller;

use App\Entity\Post;
use App\Repository\PostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\PostType;

    /**
     * @Route("/post", name="post.")
     */
class PostController extends AbstractController
{
    /**
     * @Route("/", name="index")
     * @param PostRepository $postRepository
     * @return Response
     */
    public function index(PostRepository $postRepository)
    {

        $posts = $postRepository->findAll();
        return $this->render('post/index.html.twig', [
            'posts' => $posts
        ]);
    }

    /**
     * @Route("/create", name="create")
     * @param Request $request
     * @return Response
     */
    public function create(Request $request){
        $post = new Post();
       // $post->setTitle('toks pavadinimas');
       $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);
        if($form->isSubmitted()){
            $em = $this->getDoctrine()->getManager();
            //dump($post);
            $em->persist($post);
            $em->flush();
            $this->addFlash('success', 'Post created');
            return $this->render('post/create.html.twig', ['form' => $form->createView()
       ]);
        }
       // return $this->redirect($this->generateUrl('post.index'));
       return $this->render('post/create.html.twig', ['form' => $form->createView()
       ]);
    }





    /**
     * @Route("/show/{id}", name="show")
     * @param Post $post
     * @return Response
     */
    public function show(Request $request,Post $post){
        
        $em = $this->getDoctrine()->getManager();
        
        $array =  (array) $post;
        
        $keys = array_values($array);
        implode(',',$keys);



        if($keys[2]){
            $sql = "SELECT p.id as pid,p.title as ptitle,pg.id as pid, pg.group_title as pgtitle FROM `post` p left join post_group pg on p.group_id = pg.id where p.id = " . $keys[0];
            $results = $em->getConnection()->prepare($sql);
            $results->execute();
 
            return $this->render('post/show.html.twig', ['results' => $results]);
        }else{

            $post = new Post();
            // $post->setTitle('toks pavadinimas');
            $form = $this->createForm(PostType::class, $post);
             $form->handleRequest($request);
            return $this->render('post/show.html.twig', ['post' => $post, 'form' => $form->createView()]);
           
        }
       
        
    }














     /**
     * @Route("/delete/{id}", name="delete")
     * @return Response
     */
    public function remove(Post $post){

        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();

        //redirect
        $this->addFlash('success', 'Post was removed');
        return $this->redirect($this->generateUrl('post.index'));
    }
}
