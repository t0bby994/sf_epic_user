<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

namespace App\Controller;

use App\Entity\Post;
use App\Repository\PostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\PostType;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(Request $request)
    {
    

        $em = $this->getDoctrine()->getManager();
        
        $sql = "SELECT p.id as pid,p.title as ptitle,pg.id as pid, pg.group_title as pgtitle FROM `post` p left join post_group pg on p.group_id = pg.id";
        $results = $em->getConnection()->prepare($sql);
        $results->execute();
 

        return $this->render('home/index.html.twig', ['results' => $results]);
    }


}
